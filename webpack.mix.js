const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

    let jsFiles = [
		{ 'src': '/js/app.js', 'dest': '/js' },
		{ 'src': '/js/pages/results.js', 'dest': '/js/pages' }
	];

	for (files in jsFiles) {
		mix.js('resources' + jsFiles[files].src, 'public' + jsFiles[files].dest);	
	}

	jsFiles = [
		{ 'src': '/js/pages/home.js', 'dest': '/js/pages' },
	];

	for (files in jsFiles) {
		mix.js('resources' + jsFiles[files].src, 'public' + jsFiles[files].dest);	
	}


	jsFiles = [
		{ 'src': '/js/pages/result.js', 'dest': '/js/pages' },
	];

	for (files in jsFiles) {
		mix.js('resources' + jsFiles[files].src, 'public' + jsFiles[files].dest);	
	}

    
    let cssFiles = [
		
		{ 'src': '/sass/app.scss', 'dest': '/scss' },
		{ 'src': '/sass/news.scss', 'dest': '/scss' },
	];

	for (cssfile in cssFiles) {
		mix.sass('resources' + cssFiles[cssfile].src, 'public' + cssFiles[files].dest);	
	}

	console.log("*********"+__dirname+"***********")
// exclude: /node_modules\/(?!(vue-select)\/).*/,
mix.webpackConfig({
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': __dirname + '/resources/js',
			'sass': __dirname + '/resources/sass'
		}
	}
});
