@extends('layouts.app')
@section('content')
<home-page inline-template>
	<div class="home-page">
		<carousel
			:flux="{{$flux}}"
		></carousel>
		<div class="jumbotron jumbotron-fluid">
			<div class="container">
				<h1 class="display-4">RSS SPORT</h1>
				<p class="lead">Suivez en direct les dernieres actualités du Sport des plus grands sites de sports</p>
				<!--<div class="d-flex justify-content-between p-4">
					<img width="200px" height="80px" src="https://www.lequipe.fr/rss/logo_RSS.gif">
					<img width="200px" height="80px" src="https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg):focal(1161x775:1163x773)/origin-imgresizer.eurosport.com/2015/11/12/1731141-36621046-2560-1440.jpg">
					<img width="200px" height="80px" src="https://logovtor.com/wp-content/uploads/2019/09/bfm-avec-rmc-sport-logo-vector.png">
				</div>-->
			</div>
		</div>

		<div class="container-results">
			<results-news
				:flux="{{$flux}}"
				size=9
				:show-more=true
				:redirect=true
			></results-news>
		</div>
	</div>
</home-page>
@endsection

@section('components')
	@foreach([
		'results-news'			  => 'results-news',
		'result-news'			  => 'result-news',
		'carousel'			  => 'carousel',
	] as $id => $component)
		<script v-pre type="text/x-template" id="{{$id}}">
			@include('components.'.$component)
		</script>
	@endforeach
@endsection


@section('js')
	<script src="{{ mix('js/pages/home.js') }}" defer></script>
@endsection
