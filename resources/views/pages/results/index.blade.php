@extends('layouts.app')
@section('content')
<results inline-template>
	<div class="results">
        <div class="jumbotron jumbotron-image color-grey-light" style="background-image: url('https://assets-fr.imgfoot.com/riyad-mahrez-psg.jpg'); height: 400px; padding: 0px;">
			<div style="background-color: rgba(0, 0, 0, 0.3); width: 100%; height: 100%;">
				<div class="mask rgba-black-strong d-flex align-items-center h-100">
					<div class="container text-center white-text py-5">
						<h1 class="mb-0">Actualités Sport</h1>
					</div>
				</div>
			</div>
        </div>

        <div class="container-results">
			<results-news
				:flux="{{$flux}}"
				:show-more=false
			></results-news>
		</div>

	</div>
</results>
@endsection

@section('components')
	@foreach([
		'results-news'			  => 'results-news',
		'result-news'			  => 'result-news',
		'filters'			  => 'filters',
	] as $id => $component)
		<script v-pre type="text/x-template" id="{{$id}}">
			@include('components.'.$component)
		</script>
	@endforeach
@endsection


@section('js')
	<script src="{{ mix('js/pages/results.js') }}" defer></script>
@endsection
