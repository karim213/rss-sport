@extends('layouts.app')
@section('content')
<result 
	:article="{{$article}}"
	inline-template>
	<div>
		<div class="d-flex justify-content-between p-4" style="background-color: rgba(0, 0, 0, 0.05);" > 
			<h3 class="card-title" v-html="article.title"></h3>
		</div>

		<div class="container container-article">
			<div  id="article">
				<div class="card" style="width: 100M; align-items: center">
					<img :src="article.image" class="card-image">
					<div class="card-body">
						<div class="h-3 caps-s7-fx mb-1 text-br-2-70 light:text-br-2-40 font-bold lines-1" data-testid="secondary-description-category" v-html="article.channel_title"></div>
						<p class="card-description" v-html="article.description"></p>
						<a :href="article.link">Lire sur le site</a>
					</div>
					<a :href="article.channel_link"><img :src="article.channel_image" width="240px"/></a>
				</div>
			</div>
		</div>
	</div>
</result>
@endsection

@section('components')
	@foreach([
		'results-news'			  => 'results-news',
		'result-news'			  => 'result-news',
	] as $id => $component)
		<script v-pre type="text/x-template" id="{{$id}}">
			@include('components.'.$component)
		</script>
	@endforeach
@endsection


@section('js')
	<script src="{{ mix('js/pages/result.js') }}" defer></script>
@endsection
