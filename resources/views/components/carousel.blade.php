<div style="background: linear-gradient(90deg, rgb(18 37 58) 0%, rgb(97 35 51) 35%, rgb(22 37 63) 100%);">
  <div id="carousel"> 
      <b-carousel
        controls
        indicators 
        v-model="slide"
        :interval="4000"
        controls
        indicators
        background="#ababab"
        img-width="1024"
        img-height="50%"
        style="text-shadow: 1px 1px 2px #333;"
        @sliding-end="endSlide"
        @sliding-start="initCarousel">
        
        <b-carousel-slide
          v-for="news in flux.slice(0,5)"
          :img-src="news.image"
          :caption="news.title">
        </b-carousel-slide>
        
      </b-carousel>
  </div>
</div>