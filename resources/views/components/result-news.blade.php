<div v-if="news" id="result-news">
    <div class="card"  v-on:click="showArticle">
        <div class="card-image" :style="{ 'background-image': 'url(' + news.image + ')' }">
            <div style="background-color: white; width: fit-content;">
                <img v-if="news.channel_image" :src="news.channel_image" width="80px" />
            </div>
        </div>
        <div class="card-body">
            <div class="h-3 caps-s7-fx mb-1 text-br-2-70 light:text-br-2-40 font-bold lines-1" data-testid="secondary-description-category">@{{ news.channel_title }}</div>
            <h3 class="card-title" v-html="news.title"></h3>
            <span class="card-description" v-html="news.description"></span>
        </div>
    </div>
</div>
