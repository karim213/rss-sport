<div id="results-news">
    <h2 v-if='showMore' class="news-title section-title">{{__('news.title')}}</h2>
    <filters></filters>
    <b-card-group deck class="col d-md-flex justify-content-center">

        <result-news
            v-for="news in flux.slice(showFrom,showTo)"
            :news='news'
        >
        </result-news>
    </b-card-group>
    <div class="container" style="display: flex; align-items: center;">
        <button v-if='showMore' type="button" style="margin-top : 20px; color: white; width: 200px; margin: auto;" v-on:click="showMoreResults" class="btn btn-warning btn-block">Afficher Plus</button>
    </div>
    <div v-if='!showMore' class="overflow-auto d-flex justify-content-center">
        <b-pagination
            v-model="currentPage"
            :total-rows="flux.length"
            :per-page="perPage"
        ></b-pagination>

    </div>

</div>