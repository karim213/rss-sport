<div class="header">
  <b-navbar toggleable="lg" type="light" variant="light">
    <b-navbar-brand href="/">
           <img src="{{url('/images/logo.png')}}" alt="Image"/>
    </b-navbar-brand>

    <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>

    <b-collapse id="nav-collapse" is-nav>
      <b-navbar-nav>
        <b-nav-item href="/results">Liste d'actualités</b-nav-item>
      </b-navbar-nav>
    </b-collapse>
  </b-navbar>

</div>