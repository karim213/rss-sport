<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('scss/news.css') }}" rel="stylesheet">

        <title>Rss Sport - Dernieres Actualités</title>

        <script data-ad-client="ca-pub-4204642940778363" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    </head>
    <body>
        <div id="app">
            @include('layouts.header')
            @yield('content')
            @include('layouts.footer')
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('js')
        @yield('components')
    </body>
</html>
