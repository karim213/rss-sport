import Vue from '@/app';

import ResultsNews from '@/components/ResultsNews.vue';
import ResultNews from '@/components/ResultNews.vue';
import Filters from '@/components/Filters.vue';

Vue.component('ResultsNews', ResultsNews);
Vue.component('ResultNews', ResultNews);
Vue.component('Filters', Filters);

Vue.component('results', {
    template: '#results'
});


const app = new Vue({
	el: '#app'
});

