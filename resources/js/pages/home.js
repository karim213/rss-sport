import Vue from '@/app';

import ResultsNews from '@/components/ResultsNews.vue';
import ResultNews from '@/components/ResultNews.vue';
import Carousel from '@/components/Carousel.vue';

Vue.component('ResultsNews', ResultsNews);
Vue.component('ResultNews', ResultNews);
Vue.component('Carousel', Carousel);

Vue.component('home-page', {
    template: '#home-page'
});


const app = new Vue({
	el: '#app'
});

