import Vue from '@/app';

import ResultsNews from '@/components/ResultsNews.vue';
import ResultNews from '@/components/ResultNews.vue';

Vue.component('ResultsNews', ResultsNews);
Vue.component('ResultNews', ResultNews);

Vue.component('result', {
    template: '#result',
    props:{
        article: {type : Object, required: true}
    },
});


const app = new Vue({
	el: '#app'
});

