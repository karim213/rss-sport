import Vue from 'vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

require('./bootstrap');
import { NavbarPlugin } from 'bootstrap-vue'
import { CardPlugin } from 'bootstrap-vue'
import { BButton } from 'bootstrap-vue'
import { ListGroupPlugin } from 'bootstrap-vue'
import { CarouselPlugin } from 'bootstrap-vue'
import { PaginationPlugin } from 'bootstrap-vue'

Vue.component('b-button', BButton)


Vue.use(NavbarPlugin)
Vue.use(CardPlugin)
Vue.use(ListGroupPlugin)
Vue.use(CarouselPlugin)
Vue.use(PaginationPlugin)





export default Vue;