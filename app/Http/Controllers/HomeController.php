<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class HomeController extends Controller{
    public function index(){
        $response = Http::get('http://localhost:3000/api/follow');

        return view('pages.home.index', ['flux' => $response->body()]);
    }
}