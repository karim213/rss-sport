<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ResultController extends Controller{
    public function index(Request $request){
        $articleId = $request->input('articleId');
        $response = Http::get('http://localhost:3000/api/follow', [
            'articleId' => $articleId
        ]);

        return view('pages.result.index', ['article' => $response->body()]);
    }
}