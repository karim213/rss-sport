<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class ResultsController extends Controller{
    public function index(){
        $response = Http::get('http://localhost:3000/api/follow');

        return view('pages.results.index', ['flux' => $response->body()]);
    }
}